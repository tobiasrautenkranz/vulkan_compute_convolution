#!/usr/bin/Rscript --vanilla

library("stringr")
library("reshape2")
library("scales")

library("ggplot2")
library("ggthemes")

plot_benchmark_3 <- function (df, x, y, group, dodge=0)
{
  position <- "identity"
  if (dodge) position <- "dodge"

  ggplot(df, aes_string(x=x, y=y,
  # Fill and dodge boxplots by group on a continuous x axis
  # https://stackoverflow.com/a/45108612
                        group=paste("interaction(", x, ",", group, ")", sep=""),
                        color=group)) +
    geom_line(aes_string(x=x, y=y, group=group), stat="summary_bin",
              fun.y = "mean", alpha=0.2,
            position=position_dodge(width=dodge)) +
    geom_boxplot(position=position, fill=NA, outlier.size=0, outlier.color=NA) +
    geom_point(position=position_jitterdodge(dodge.width=dodge), alpha=0.4) +
    scale_x_sqrt(breaks=unique(df[,x])) +
    scale_color_discrete(name=NULL)
}

FLOP <- function(inputsize, kernelsize)
{
  outputsize <- inputsize - (kernelsize -1)
  multiplications_per_output <- kernelsize * kernelsize
  additions_per_output <- kernelsize * kernelsize -1
  outputsize * outputsize * (multiplications_per_output + additions_per_output)
}

FLOP_same <- function(inputsize, kernelsize)
{
  outputsize <- inputsize
  multiplications_per_output <- kernelsize * kernelsize
  additions_per_output <- kernelsize * kernelsize -1
  outputsize * outputsize * (multiplications_per_output + additions_per_output)
}


vulkan <- read.csv("benchmark_vulkan.csv")

vulkan$bytes_per_second <- NULL
vulkan$items_per_second <- NULL
vulkan$label <- NULL
vulkan$error_occurred <- NULL
vulkan$error_message <- NULL

labels <- str_split(vulkan$name, "/", 3, TRUE)
vulkan$benchmark <- factor(labels[,1])
vulkan$inputsize <- as.numeric(labels[,2])
vulkan$kernelsize <- as.numeric(labels[,3])

vulkan <- na.omit(vulkan)

vulkan$real_time_s <- vulkan$real_time / 1e9; # from ns

vulkan$FLOPS <- do.call(FLOP, list(vulkan$inputsize, vulkan$kernelsize)) /
  vulkan$real_time_s

CPU <- vulkan[vulkan$benchmark == "CPU_Convolution",]

vulkan$FLOPS_relative <- vulkan$FLOPS / mean(CPU$FLOPS)

# reorder
vulkan$benchmark <- factor(vulkan$benchmark,
                           levels = c("GPU_Convolution", "CPU_Convolution"))

#pdf()
svg()

theme_set(theme_solarized(base_size = 20, light=FALSE)
          + theme(legend.background=element_rect(colour=NA, fill=NA),
                  legend.key=element_rect(colour=NA, fill=NA),
                  strip.background = element_rect(fill="grey30")
                  ))


print(plot_benchmark_3(vulkan, "inputsize", "FLOPS_relative", "benchmark") +
  labs(x = "input size", y = "realtive FLOPS") +
  scale_color_discrete(name=NULL, labels=c("Vulkan","CPU")) +
  facet_wrap(~ kernelsize))

opencl_benchmark_filename <- "convolution_cl/benchmark_cl.csv"

if (file.exists(opencl_benchmark_filename)) {
  opencl_data <- read.csv(opencl_benchmark_filename)

  opencl <- data.frame(benchmark = opencl_data$benchmark,
                       inputsize = opencl_data$Width,
                       kernelsize = opencl_data$FilterSize,
                       real_time_s = opencl_data$KernelTime.sec)
  opencl_data <- NULL

  opencl$FLOPS <- do.call(FLOP_same, list(opencl$inputsize, opencl$kernelsize)) /
    opencl$real_time_s

  cl_vulkan <- merge(vulkan, opencl, all = TRUE)
  cl_vulkan <- cl_vulkan[cl_vulkan$inputsize == 512,]

  levels(cl_vulkan$benchmark)[levels(cl_vulkan$benchmark) == "GPU_Convolution" ] <-  "Vulkan"
  levels(cl_vulkan$benchmark)[levels(cl_vulkan$benchmark) == "SimpleConvolution" ] <-  "OpenCL"
  levels(cl_vulkan$benchmark)[levels(cl_vulkan$benchmark) == "AdvancedConvolution" ] <-  "OpenCL LDS"

  CL <- cl_vulkan[cl_vulkan$benchmark == "OpenCL",]
  cl_vulkan <- cl_vulkan[cl_vulkan$benchmark != "CPU_Convolution",]

  cl_vulkan$FLOPS_relative <- cl_vulkan$FLOPS / mean(CL$FLOPS)

  print(plot_benchmark_3(cl_vulkan, "kernelsize", "FLOPS_relative", "benchmark", 0.4) +
    labs(x = "kernel size", y = "realtive FLOPS"))

  cl_vulkan_1 <- cl_vulkan[cl_vulkan$benchmark == "Vulkan" |
                           cl_vulkan$benchmark == "OpenCL LDS", ]
  cl_vulkan_1$kernelarea <- cl_vulkan_1$kernelsize * cl_vulkan_1$kernelsize


  vulkan <- cl_vulkan[cl_vulkan$benchmark == "Vulkan",]
  CL_LDS <- cl_vulkan[cl_vulkan$benchmark == "OpenCL LDS",]

  vulkan_5 <- vulkan[vulkan$kernelsize == 5,]
  CL_LDS_5 <- CL_LDS[CL_LDS$kernelsize == 5,]
  CL_5 <- CL[CL$kernelsize == 5,]

  cat("relative speed vulkan (5) to CL LDS: ",
      round(mean(vulkan_5$FLOPS)/mean(CL_LDS_5$FLOPS)*100), "%\n")
  cat("relative speed CL LDS to CL: ",
      round(mean(CL_LDS_5$FLOPS)/mean(CL_5$FLOPS)*100), "%\n")

  #model <- lm(FLOPS ~ benchmark + kernelarea, cl_vulkan_1)
  #print(summary(model))
  #print(anova(model))

  #kruskal.test(FLOPS ~ benchmark, data=cl_vulkan_1)
  #bartlett.test(FLOPS ~ interaction(benchmark, kernelarea), data=cl_vulkan_1)

  #print(oneway.test(FLOPS ~ benchmark + kernelarea, data=cl_vulkan_1))
  #print(kruskal.test(FLOPS ~ interaction(benchmark,kernelarea), data=cl_vulkan_1))

  #library("car")
  #print(leveneTest(FLOPS ~ interaction(benchmark, kernelarea), data=cl_vulkan_1))

  print(plot_benchmark_3(cl_vulkan_1, "kernelsize", "real_time_s", "benchmark", 0.4) +
    labs(x = "kernel size", y = "real time [s]"))
}

dev.off()
