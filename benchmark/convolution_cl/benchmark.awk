BEGIN{
  OFS = ",";
  header=1;
  quote="\"";
}

/\| /{
  if(header) {
    header = 0;
    $6="benchmark";
    if (h == "true")
      print $2,$3,$4,$5,"benchmark";
  }
  else
    print $2,$3,$4,$5,(quote conv quote);
}

/^ Separable/{
  exit
}
