#!/bin/bash
set -euo pipefail

if [ $# -ne 1 ]; then
  echo "usage: $0 BIN_PATH" >&2
  exit 1
fi

BIN_PATH=$1

samples=10
iteration_count=200
printHeader=true


for conv in AdvancedConvolution SimpleConvolution; do
  for kernelSize in 3 5; do
    for sample in $(seq $samples); do
      "${BIN_PATH}/${conv}" -t -m $kernelSize -i "$iteration_count" \
        | awk -F "|" -v conv="$conv" -v h="$printHeader" \
        -f benchmark.awk | tr -d " "
      printHeader=false
    done;
  done;
done;
